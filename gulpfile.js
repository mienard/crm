'use strict';

const { src, dest, series, watch } = require('gulp');
const sass = require('gulp-sass');
const rename = require('gulp-rename');
const bs = require('browser-sync').create();
const npmDist = require('gulp-npm-dist');
const htmlInjector = require('bs-html-injector');

sass.compiler = require('node-sass');

// Compile scss files to core.css file
function compileStyle() {
  return src('./scss/core.scss')
  .pipe(sass().on('error', sass.logError))
  .pipe(dest('./assets/css'))
  .pipe(bs.stream());
}

// Compile scss files to core.css file
function compileStyleMain() {
  return src('./scss/main.scss')
  .pipe(sass().on('error', sass.logError))
  .pipe(dest('./assets/css'))
  .pipe(bs.stream());
}

// Compile and minify scss files to core.min.css file
function minifyStyle () {
  return src('./scss/core.scss')
    .pipe(sass({outputStyle: 'compressed'}))
    .pipe(rename({suffix: '.min'}))
    .pipe(dest('./assets/css'))
    .pipe(bs.stream());
}

// Compile and minify scss files to core.min.css file
function minifyStyleMain () {
  return src('./scss/main.scss')
    .pipe(sass({outputStyle: 'compressed'}))
    .pipe(rename({suffix: '.min'}))
    .pipe(dest('./assets/css'))
    .pipe(bs.stream());
}

// Start a server
function serve () {
  bs.use(htmlInjector, {
    files: "**/*.html"
  });

  // Now init the Browsersync server
  bs.init({
    injectChanges: true,
    server: true
  });

  // Listen to change events on HTML and reload
  watch('**/*.html').on('change', htmlInjector);

  // Provide a callback to capture ALL events to CSS
  // files - then filter for 'change' and reload all
  // css files on the page.
  watch('scss/_mixins.scss', series(compileStyle, compileStyleMain, minifyStyle, minifyStyleMain));
  watch('scss/_variables.scss', series(compileStyle, compileStyleMain, minifyStyle, minifyStyleMain));

  watch('scss/components/*.scss', series(compileStyle, minifyStyle));
  watch('scss/plugins/*.scss', series(compileStyle, minifyStyle));
  watch('scss/core.scss', series(compileStyle, minifyStyle));

  watch('scss/pages/*.scss', series(compileStyleMain, minifyStyleMain));
  watch('scss/panels/*.scss', series(compileStyleMain, minifyStyleMain));
  watch('scss/main.scss', series(compileStyleMain, minifyStyleMain));
}

// Copy dependencies to template/lib
function npmDep () {
  return src(npmDist(), { base:'./node_modules/' })
    .pipe(rename(function(path) {
      path.dirname = path.dirname.replace(/\/dist/, '').replace(/\\dist/, '');
    }))
    .pipe(dest('./lib'));
}

exports.compileStyle = compileStyle;
exports.compileStyleMain = compileStyleMain;

exports.minifyStyle = minifyStyle;
exports.minifyStyleMain = minifyStyleMain;

exports.serve = serve;
exports.npmDep = npmDep;
